﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using TrueKitchen.Infra.Api.Middleware.Authorization;
using TrueKitchen.Infra.Api.Middleware.Exception;
using TrueKitchen.Infra.Api.Middleware.Headers;
using TrueKitchen.Infra.Api.Middleware.HealthCheck;
using TrueKitchen.Infra.Api.Middleware.Metrics;

namespace TrueKitchen.Infra.Api.Tests.Helpers
{
    public class TestStartup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        public TestStartup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        //this method gets called at runtime
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
           

            //order is important
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseMiddleware<ErrorLoggingMiddleware>();
            app.UseMiddleware<HealthCheckMiddleware>();
            app.UseMiddleware<TrackingRequestResponseMiddleware>();
            app.UseMiddleware<PerformanceMetricsMiddleware>();

            app.UseAuthentication();
            app.UseMvc(routes => { routes.MapRoute(name: "default", template: "api/{controller}/{action}/{id?}"); });
        }

        //this method gets called at runtime.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddTrueKichenAuthorization(new List<string> { "All" });
            services.AddMvc();
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);

            containerBuilder.RegisterType<LoggerFactory>()
                 .As<ILoggerFactory>()
                 .SingleInstance();

            containerBuilder.Register<ILogger>(c =>
            {
                var loggerFactory = c.Resolve<ILoggerFactory>();
                return loggerFactory.CreateLogger("TrueKitchen.Infra.Api.Tests");
            });

            containerBuilder.RegisterType<TestAuthenticationService>()
                .AsImplementedInterfaces();
                

            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }
    }
}
