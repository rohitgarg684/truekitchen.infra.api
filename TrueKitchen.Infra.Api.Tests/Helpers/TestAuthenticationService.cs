﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TrueKitchen.Infra.Api.Filters.Interfaces;

namespace TrueKitchen.Infra.Api.Tests.Helpers
{
    public class TestAuthenticationService : IAuthenticationService
    {
        public Task<bool> IsUserAuthorizedWithAnyOfTypes(string token, IList<string> userTypes)
        {
            return Task.FromResult<bool>(true);
        }
    }
}
