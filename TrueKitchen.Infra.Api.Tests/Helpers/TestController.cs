﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TrueKitchen.Infra.Api.Middleware.Authorization;
using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Infra.Api.Tests.Helpers
{
    public class TestController: Controller
    {
        public TestController()
        {
        }

        public IEnumerable<string> Values()
        {
            return new List<string> { "value1", "value2" };
        }

        public IEnumerable<string> ThrowEx()
        {
            throw new Exception("Test");
        }

        public IEnumerable<string> ThrowBaseEX()
        {
            var errors = new List<Error> { new Error(192, "Test Error from test solution")};
            throw new BaseException(errors, System.Net.HttpStatusCode.BadRequest);
        }

        [Authorize(AuthPolicy.UserLoggedIn)]
        public bool Secured()
        {
            return true;
        }
    }
}
