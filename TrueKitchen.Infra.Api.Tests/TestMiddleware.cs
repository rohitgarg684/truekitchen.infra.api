﻿using Microsoft.AspNetCore.TestHost;
using System.Threading.Tasks;
using TrueKitchen.Infra.Api.Middleware.Headers;
using Xunit;

namespace TrueKitchen.Infra.Api.Tests
{
    public class TestMiddleware
    {
        private readonly TestServer _testServer;
        public TestMiddleware()
        {
            _testServer = Helpers.TestServerFactory.GetServer();
        }

        [Fact]
        public void Test_CorrelationId_Middleware()
        {
            var client = _testServer.CreateClient();
            var response = client.GetAsync(client.BaseAddress +"api/test/values").Result;
            Assert.True(response.Headers.Contains(HeaderConstants.CorrelationIdHeader));
        }

        [Fact]
        public void Test_ExceptionLogging_Middleware()
        {
            var client = _testServer.CreateClient();
            var response = client.GetAsync(client.BaseAddress + "api/test/throwEx").Result;
        }

        [Fact]
        public async Task Test_CheckedExceptionLogging_Middleware()
        {
            var client = _testServer.CreateClient();
            var response = client.GetAsync(client.BaseAddress + "api/test/ThrowBaseEX").Result;
            var error = await response.Content.ReadAsStringAsync();
        }

        [Fact]
        public void Test_Auth_Middleware()
        {
            var client = _testServer.CreateClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "some random JWT token");
            var response = client.GetAsync(client.BaseAddress + "api/test/Secured").Result;
        }

        [Fact]
        public void Test_HealthCheck()
        {
            var client = _testServer.CreateClient();
            var response = client.GetAsync(client.BaseAddress + "hc").Result;
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);
        }
    }

}
