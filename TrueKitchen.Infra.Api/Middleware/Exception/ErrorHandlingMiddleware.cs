﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Truekitchen.Infra.Core.Exceptions.DomainException;
using TrueKitchen.Infra.Api.Middleware.Headers;
using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Infra.Api.Middleware.Exception
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IHostingEnvironment _env;

        public ErrorHandlingMiddleware(RequestDelegate next, IHostingEnvironment env)
        {
            _next = next;
            _env = env;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch(System.Exception ex)
            {
               await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, System.Exception ex)
        {
            if (ex is BaseException)
            {
                var exception = ex as BaseException;
                var result= new { Errors = exception.Errors };
                context.Response.StatusCode = (int)exception.HttpStatusCode;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                return ;
            }

            if (DomainException.IsDomainException(ex))
            {
                var response = DomainException.CreateResponse(ex);
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                var result = new { Errors = response };
                await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                return ;
            }

            if (!_env.IsDevelopment())
            {
                context.Request.Headers.TryGetValue(HeaderConstants.CorrelationIdHeader, out var correlationIdValues);
                var correlationId = correlationIdValues.Any() ? correlationIdValues.First() : "";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                var result = new { Errors = $"Some error occured. Please use this id : {correlationId}  to get more details." };
                await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                return ;
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(ex.ToString()));
            }

        }
    }
}
