﻿using System.Net;
using System.Threading.Tasks;
using Truekitchen.Infra.Core.Exceptions.DomainException;
using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Infra.Api.Middleware.Exception
{
    public class ExceptionUtils
    {
        public static async Task<bool> IsCheckedException(System.Exception ex)
        {
            if (ex is BaseException || DomainException.IsDomainException(ex))
                return true;

            return false;
        }

        public static async Task<int> GetHttpStatusCode(System.Exception ex)
        {
            if (ex is BaseException)
            {
                var exception = ex as BaseException;
                return (int)exception.HttpStatusCode;
            }
            if (DomainException.IsDomainException(ex))
            {
                return (int)HttpStatusCode.BadRequest;
            }
            else
            {
                return (int)HttpStatusCode.InternalServerError;
            }
        }
    }
}
