﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Truekitchen.Infra.Core.Exceptions.DomainException;
using TrueKitchen.Infra.Api.Middleware.Headers;
using TrueKitchen.Infra.Core.Exceptions;

namespace TrueKitchen.Infra.Api.Middleware.Exception
{
    public class ErrorLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _log;

        public ErrorLoggingMiddleware(RequestDelegate next, ILogger log)
        {
            _next = next;
            _log=log;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (System.Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
                throw;
            }
        }
        private async Task HandleExceptionAsync(HttpContext context, System.Exception ex)
        {
            if (ex is BaseException || DomainException.IsDomainException(ex))
                return;

            var errorInfo = await CreateErrorResponse(context,ex);
            var response = JsonConvert.SerializeObject(errorInfo);
            _log.LogError(response);
        }

        private static async Task<HttpRequestModel> CreateErrorResponse(HttpContext context, System.Exception ex)
        {
            var correlationId = await HeadersUtil.GetCorrelationIdFromRequest(context);

            var request = new HttpRequestModel
            {
                Body =ex.ToString(),
                Method = context.Request.Method,
                Scheme = context.Request.Scheme,
                Host = context.Request.Host.ToString(),
                Protocol = string.Empty,
                PathBase = context.Request.PathBase,
                Path = context.Request.Path,
                QueryString = context.Request.QueryString.ToString(),
                CorrelationId = correlationId.Any()? correlationId.First():""
            };

            return request;
        }
    }
}
