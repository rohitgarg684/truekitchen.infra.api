﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System.Threading.Tasks;

namespace TrueKitchen.Infra.Api.Middleware.Headers
{
    public class HeadersUtil
    {
        public static async Task<StringValues> GetCorrelationIdFromRequest(HttpContext context)
        {
            context.Request.Headers.TryGetValue(HeaderConstants.CorrelationIdHeader, out var correlationId);
            return correlationId;
        }

    }
}
