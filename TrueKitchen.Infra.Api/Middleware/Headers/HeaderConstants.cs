﻿namespace TrueKitchen.Infra.Api.Middleware.Headers
{
    public class HeaderConstants
    {
        public const string CorrelationIdHeader = "x-correlation-id";
    }
}
