﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace TrueKitchen.Infra.Api.Middleware.Headers
{
    public class TrackingRequestResponseMiddleware
    {
        private readonly RequestDelegate _next;

        public TrackingRequestResponseMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var correlationIdValues = await HeadersUtil.GetCorrelationIdFromRequest(httpContext);
            string correlationId = string.Empty;

            if (!correlationIdValues.Any())
            {
                correlationId = Guid.NewGuid().ToString();
                httpContext.Request.Headers.Add(HeaderConstants.CorrelationIdHeader, new[] { correlationId });
            }
            else
                correlationId = correlationIdValues.First();

            httpContext.Response.OnStarting(() => {
                httpContext.Response.Headers.Add(HeaderConstants.CorrelationIdHeader, new[] { correlationId });
                return Task.FromResult(0);
            });
            

            await _next(httpContext);
        }
    }
}
