﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueKitchen.Infra.Api.Filters.Interfaces;

namespace TrueKitchen.Infra.Api.Middleware.Authorization
{
    public class LoggedInUserRequirementHandler:AuthorizationHandler<LoggedInUserRequirement>, IAuthorizationRequirement
    {
        private readonly IAuthenticationService _authenticationService;
       
        public LoggedInUserRequirementHandler(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, LoggedInUserRequirement requirement)
        {
            // 1. Look for credentials in the request.
            var authorizationFilterContext = context.Resource as AuthorizationFilterContext;
            var request = authorizationFilterContext.HttpContext.Request;

            // 2. If there are no credentials, do nothing. or If there are credentials but the filter does not recognize the 
            //    authentication scheme, do nothing.

            var token = AuthUtil.GetAuthTokenFromRequest(request);
            if (string.IsNullOrEmpty(token))
            {
                authorizationFilterContext.Result = new ObjectResult("Credentials Missing");
                return;
            }
            
            if (!await _authenticationService.IsUserAuthorizedWithAnyOfTypes(token, requirement.UserTypes))
            {
                authorizationFilterContext.Result = new ObjectResult("Invalid Credentials");
                return;
            }

            context.Succeed(requirement);
        }
    }
}
