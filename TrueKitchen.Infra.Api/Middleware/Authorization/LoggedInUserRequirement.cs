﻿using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace TrueKitchen.Infra.Api.Middleware.Authorization
{
    public class LoggedInUserRequirement:IAuthorizationRequirement
    {
        public IList<string> UserTypes { get; private set; }
        public LoggedInUserRequirement(IList<string> userTypes)
        {
            UserTypes = userTypes;
        }
    }
}
