﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace TrueKitchen.Infra.Api.Middleware.Authorization
{
    public static class AuthorizationExtensions
    {
        public static void AddTrueKichenAuthorization(this IServiceCollection services, IList<string> userTypes)
        {

            services.AddAuthorization(options => 
            {
                options.AddPolicy(AuthPolicy.UserLoggedIn,
                policy =>
                {
                    policy.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme);
                    policy.Requirements.Add(new LoggedInUserRequirement(userTypes));
                });
            });

            services.AddScoped<IAuthorizationHandler, LoggedInUserRequirementHandler>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer();
        }
    }
}
