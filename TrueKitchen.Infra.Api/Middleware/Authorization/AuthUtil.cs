﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace TrueKitchen.Infra.Api.Middleware.Authorization
{
    public class AuthUtil
    {
        private const string Bearer = "Bearer";
        private const string AuthorizationHeaderName = "Authorization";

        public static string GetAuthTokenFromRequest(HttpRequest request)
        {
            if (!request.Headers.TryGetValue(AuthorizationHeaderName, out var authorization))
                return null;

            if (!authorization.First().StartsWith(Bearer))
                return null;

            var token = authorization.First().Replace(Bearer, "").Trim();
            return token;
        }
    }
}
