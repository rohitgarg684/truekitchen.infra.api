﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace TrueKitchen.Infra.Api.Middleware.HealthCheck
{
    public class HealthCheckMiddleware
    {
        private readonly RequestDelegate _next;

        public HealthCheckMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value == "/hc")
            {
                context.Response.StatusCode = 200;
                context.Response.ContentLength = 2;
                await context.Response.WriteAsync("up");
            }
            else
            {
                await _next(context);
            }
        }
    }
}
