﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TrueKitchen.Infra.Api.Filters.Interfaces
{
    public interface IAuthenticationService
    {
        Task<bool> IsUserAuthorizedWithAnyOfTypes(string token, IList<string> userTypes);
    }
}
