﻿namespace TrueKitchen.Infra.Api.Middleware.Metrics
{
    internal class PerformanceMetricsModel
    {
        public string Method { get; set; }
        public string RequestUrl { get; set; }
        public int ResponseStatusCode { get; set; }
        public long ResponseTime { get; set; }
        public string CorrelationId { get; set; }
    }
}
