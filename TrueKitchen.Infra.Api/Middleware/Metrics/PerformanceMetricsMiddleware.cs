﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading.Tasks;
using TrueKitchen.Infra.Api.Middleware.Headers;
using System.Linq;
using System.Net;
using TrueKitchen.Infra.Api.Middleware.Exception;
using Microsoft.Extensions.Logging;

namespace TrueKitchen.Infra.Api.Middleware.Metrics
{
    public class PerformanceMetricsMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _log;

        public PerformanceMetricsMiddleware(RequestDelegate next, ILogger log)
        {
            _next = next;
            _log = log;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            int httpStatusCode = (int)HttpStatusCode.InternalServerError;
            try
            {
                await _next(httpContext);
            }

            catch(System.Exception ex)
            {
                httpStatusCode = await ExceptionUtils.GetHttpStatusCode(ex);
                throw;
            }

            finally
            {
                await LogMetricsAsync(httpContext, sw, httpStatusCode);
            }

        }

        private async Task LogMetricsAsync(HttpContext httpContext, Stopwatch sw, int httpStatusCode)
        {
            var responseTime = sw.ElapsedMilliseconds;
            var performanceMetricsModel = new PerformanceMetricsModel();


            performanceMetricsModel.Method = httpContext.Request.Method;
            performanceMetricsModel.RequestUrl = httpContext.Request.GetDisplayUrl();
            performanceMetricsModel.ResponseStatusCode = httpStatusCode;
            performanceMetricsModel.ResponseTime = responseTime;
            var correlationIdValues = await HeadersUtil.GetCorrelationIdFromRequest(httpContext);
            performanceMetricsModel.CorrelationId = correlationIdValues.Any() ? correlationIdValues.First() : "";

            var response = JsonConvert.SerializeObject(performanceMetricsModel);
            _log.LogInformation($"Performance metrics : {response}");
        }
    }
}
