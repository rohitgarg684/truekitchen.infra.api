﻿using Autofac;
using Microsoft.Extensions.Configuration;

namespace TrueKitchen.Infra.Api.AutofacExtensions
{
    public static class BuilderExtensions
    {
        public static void RegisterAppsettings<T>(this ContainerBuilder builder, IConfiguration fileConfig) where T : class
        {
            var parameters = (T)System.Activator.CreateInstance<T>();
            fileConfig.GetSection(typeof(T).Name).Bind(parameters);
            builder.Register<T>(c => parameters)
                .AsSelf()
                .SingleInstance();
        }
    }
}
